@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        @include('inc.messeges')
        <article id="movies" class="mb-5">
            <h2>Top Rated Movies <a href="/movies" class="btn btn-primary text-white mb-1">Show all <i class="fas fa-angle-right"></i></a></h2>
            <section class="row bg-white pt-4 rounded shadow">
                @if (count($movies) > 0)
                    @foreach ($movies as $movie)
                    <div class="col-lg-3 col-6 mb-4 text-center ">
                        <a class="media-link" href="/media/{{$movie->id}}">
                            <img class="col-12 mb-3 img-poster w-100" src="storage/cover_images/{{$movie->image}}" alt="{{$movie->title}}">
                            <h5><b>{{$movie->title}}</b></h5>
                            <p><i class="fas fa-star text-primary"></i> {{$movie->rating}}</p>
                        </a>
                        @if (count(App\Watchlist::where('user_id', Auth::user()->id)->where('media_id', $movie->id)->get()) > 0)
                            <form class="m-0 p-0" action="watchlist/{{App\Watchlist::where('user_id', Auth::user()->id)->where('media_id', $movie->id)->first()->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="url" value="/">
                                <input type="hidden" name="media_id" value="{{$movie->id}}">
                                <button type="submit" class="btn btn-primary text-white col-11" data-toggle="tooltip" data-placement="bottom" title="Remove from watchlist"><i class="fas fa-minus-square"></i> Watchlist</button>
                            </form>
                        @else
                            <form class="m-0 p-0" action="watchlist" method="POST">
                                @csrf
                                <input type="hidden" name="url" value="/">
                                <input type="hidden" name="media_id" value="{{$movie->id}}">
                                <button type="submit" class="btn btn-primary text-white col-11" data-toggle="tooltip" data-placement="bottom" title="Add to watchlist"><i class="fas fa-plus"></i> Watchlist</a>
                            </form>
                        @endif
                    </div>
                    @endforeach
                @endif
            </section>
        </article>

        <article id="series" class="mb-5">
            <h2>Top Rated TV Series <a href="/series" class="btn btn-primary text-white mb-1">Show all <i class="fas fa-angle-right"></i></a></h2>
            <section class="row bg-white pt-4 rounded shadow">
                @if (count($series) > 0)
                    @foreach ($series as $serial)
                    <div class="col-lg-3 col-6 mb-4 text-center ">
                        <a class="media-link" href="/media/{{$serial->id}}">
                            <img class="col-12 mb-3 img-poster w-100" src="storage/cover_images/{{$serial->image}}" alt="{{$serial->title}}">
                            <h5><b>{{$serial->title}}</b></h5>
                            <p><i class="fas fa-star text-primary"></i> {{$serial->rating}}</p>
                        </a>
                        @if (count(App\Watchlist::where('user_id', Auth::user()->id)->where('media_id', $serial->id)->get()) > 0)
                            <form class="m-0 p-0" action="watchlist/{{App\Watchlist::where('user_id', Auth::user()->id)->where('media_id', $serial->id)->first()->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="url" value="/#series">
                                <input type="hidden" name="media_id" value="{{$serial->id}}">
                                <button type="submit" class="btn btn-primary text-white col-11" data-toggle="tooltip" data-placement="bottom" title="Remove from watchlist"><i class="fas fa-minus-square"></i> Watchlist</button>
                            </form>
                        @else
                            <form class="m-0 p-0" action="watchlist" method="POST">
                                @csrf
                                <input type="hidden" name="url" value="/#series">
                                <input type="hidden" name="media_id" value="{{$serial->id}}">
                                <button type="submit" class="btn btn-primary text-white col-11" data-toggle="tooltip" data-placement="bottom" title="Add to watchlist"><i class="fas fa-plus"></i> Watchlist</a>
                            </form>
                        @endif
                    </div>
                    @endforeach
                @endif
            </section>
        </article>

        <article id="games" class="mb-5">
            <h2>Top Rated Games <a href="/games" class="btn btn-primary text-white mb-1">Show all <i class="fas fa-angle-right"></i></a></h2>
            <section class="row bg-white pt-4 rounded shadow">
                @if (count($games) > 0)
                    @foreach ($games as $game)
                    <div class="col-lg-3 col-6 mb-4 text-center ">
                        <a class="media-link" href="/media/{{$game->id}}">
                            <img class="col-12 mb-3 img-poster w-100" src="storage/cover_images/{{$game->image}}" alt="{{$game->title}}">
                            <h5><b>{{$game->title}}</b></h5>
                            <p><i class="fas fa-star text-primary"></i> {{$game->rating}}</p>
                        </a>
                        @if (count(App\Watchlist::where('user_id', Auth::user()->id)->where('media_id', $game->id)->get()) > 0)
                            <form class="m-0 p-0" action="watchlist/{{App\Watchlist::where('user_id', Auth::user()->id)->where('media_id', $game->id)->first()->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="url" value="/#games">
                                <input type="hidden" name="media_id" value="{{$game->id}}">
                                <button type="submit" class="btn btn-primary text-white col-11" data-toggle="tooltip" data-placement="bottom" title="Remove from watchlist"><i class="fas fa-minus-square"></i> Watchlist</button>
                            </form>
                        @else
                            <form class="m-0 p-0" action="watchlist" method="POST">
                                @csrf
                                <input type="hidden" name="url" value="/#games">
                                <input type="hidden" name="media_id" value="{{$game->id}}">
                                <button type="submit" class="btn btn-primary text-white col-11" data-toggle="tooltip" data-placement="bottom" title="Add to watchlist"><i class="fas fa-plus"></i> Watchlist</a>
                            </form>
                        @endif
                    </div>
                    @endforeach
                @endif
            </section>
        </article>

        <article id="books" class="mb-5">
            <h2>Top Rated Books <a href="/books" class="btn btn-primary text-white mb-1">Show all <i class="fas fa-angle-right"></i></a></h2>
            <section class="row bg-white pt-4 rounded shadow">
                @if (count($books) > 0)
                    @foreach ($books as $book)
                    <div class="col-lg-3 col-6 mb-4 text-center ">
                        <a class="media-link" href="/media/{{$book->id}}">
                            <img class="col-12 mb-3 img-poster w-100" src="storage/cover_images/{{$book->image}}" alt="{{$book->title}}">
                            <h5><b>{{$book->title}}</b></h5>
                            <p><i class="fas fa-star text-primary"></i> {{$book->rating}}</p>
                        </a>
                        @if (count(App\Watchlist::where('user_id', Auth::user()->id)->where('media_id', $book->id)->get()) > 0)
                            <form class="m-0 p-0" action="watchlist/{{App\Watchlist::where('user_id', Auth::user()->id)->where('media_id', $book->id)->first()->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="url" value="/#books">
                                <input type="hidden" name="media_id" value="{{$book->id}}">
                                <button type="submit" class="btn btn-primary text-white" data-toggle="tooltip" data-placement="bottom" title="Remove from watchlist"><i class="fas fa-minus-square"></i> Watchlist</button>
                            </form>
                        @else
                            <form class="m-0 p-0" action="watchlist" method="POST">
                                @csrf
                                <input type="hidden" name="url" value="/#books">
                                <input type="hidden" name="media_id" value="{{$book->id}}">
                                <button type="submit" class="btn btn-primary text-white" data-toggle="tooltip" data-placement="bottom" title="Add to watchlist"><i class="fas fa-plus"></i> Watchlist</a>
                            </form>
                        @endif
                    </div>
                    @endforeach
                @endif
            </section>
        </article>

    </div>
@endsection