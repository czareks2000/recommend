@extends('layouts.app')

@section('content')
<div class="container mt-5">
    @include('inc.messeges')
    <article class="mb-5"> 
        <h2>Watchlist</h2>
        <section class="bg-white rounded shadow px-4 pt-2 pb-4">
            @if (count($watchlist) > 0)
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="d-none d-lg-block" style="width: 1px; white-space: nowrap; border-top: none"></th>
                                <th style="border-top: none; "></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($watchlist as $item)
                            <tr>
                                <td class="m-0 p-0 d-none d-lg-block"><a class="media-link" href="/media/{{$item->id}}"><img class="img-poster" width="180" src="storage/cover_images/{{$item->image}}" alt="{{$item->title}}"></a></td>
                                <td class="p-4 align-center">
                                    <h4>
                                        <a class="media-link" href="/media/{{$item->id}}">
                                            <b>{{$item->title}}</b> <i class="fas fa-star text-primary"></i> {{$item->rating}}<br>
                                            <small>{{$item->director}}</small>
                                        </a>
                                    </h4>
                                    <p>
                                        <b>Genre:</b> {{$item->name}}
                                    </p>
                                    <p>
                                        {{$item->description}}
                                    </p>
                                    <div class="d-flex">
                                        <a class="btn btn-primary text-white" data-toggle="tooltip" data-placement="bottom" title="Open in new tab" href="{{$item->link}}" target="_blank">Source <i class="fas fa-location-arrow"></i></a>
                                        @if (($review = App\Review::where('media_id', $item->id)->where('user_id', Auth::user()->id)->first()) == NULL)
                                            <a class="btn btn-primary text-white ml-1" data-toggle="tooltip" data-placement="bottom" title="Write a review" href="/media/{{$item->id}}/review">Review <i class="fas fa-star"></i></a>
                                        @else
                                            <a class="btn btn-primary text-white ml-1" data-toggle="tooltip" data-placement="bottom" title="Edit review" href="review/{{$review->id}}/edit">Reviewed <i class="fas fa-star"></i></a>
                                        @endif
                                        <form class="m-0 p-0 ml-1" action="watchlist/{{App\Watchlist::where('user_id', Auth::user()->id)->where('media_id', $item->id)->first()->id}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <input type="hidden" name="url" value="/watchlist">
                                            <input type="hidden" name="media_id" value="{{$item->id}}">
                                            <button class="btn btn-primary text-white" data-toggle="tooltip" data-placement="bottom" title="Remove from watchlist" type="submit">Remove <i class="fas fa-trash-alt"></i></a>
                                        </form>                      
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <p class="mt-4"><i>Your watchlistis empty. Add titles</i></p>
            @endif
        </section>
    </article>
</div>
@endsection