@extends('layouts.app')

@section('content')
<div class="container mt-5">
    @include('inc.messeges')
    @foreach ($genres as $genre)
        @if (count(App\Media::where('mediaType_id', 3)->where('genre_id', $genre->id)->get()) > 0)
        <article class="mb-5"> 
            <h2>{{$genre->name}}</h2>
            <section class="row bg-white rounded shadow px-4 pt-2 pb-4">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="width: 1px; white-space: nowrap; border-top: none"></th>
                            <th style="border-top: none">Title</th>
                            <th style="border-top: none">Developer</th>
                            <th style="border-top: none">Rating</th>
                            <th style="width: 1px; white-space: nowrap; border-top: none"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($games as $game)
                        @if ($game->genre_id == $genre->id)
                        <tr>
                            <td class="m-0 p-0"><a class="media-link" href="/media/{{$game->id}}"><img class="img-poster" width="60" src="storage/cover_images/{{$game->image}}" alt="{{$game->title}}"></a></td>
                            <td class="align-middle"><a class="media-link" href="/media/{{$game->id}}">{{$game->title}}</a></td>
                            <td class="align-middle">{{$game->director}}</td>
                            <td class="align-middle"><i class="fas fa-star text-primary"></i> {{$game->rating}}</td>
                            <td class="align-middle">
                                @if (count(App\Watchlist::where('user_id', Auth::user()->id)->where('media_id', $game->id)->get()) > 0)
                                    <form class="m-0 p-0" action="watchlist/{{App\Watchlist::where('user_id', Auth::user()->id)->where('media_id', $game->id)->first()->id}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="hidden" name="url" value="/games">
                                        <input type="hidden" name="media_id" value="{{$game->id}}">
                                        <button type="submit" class="btn btn-primary text-white" data-toggle="tooltip" data-placement="right" title="Remove from watchlist"><i class="fas fa-minus-square"></i> Watchlist</button>
                                    </form>
                                @else
                                    <form class="m-0 p-0" action="watchlist" method="POST">
                                        @csrf
                                        <input type="hidden" name="url" value="/games">
                                        <input type="hidden" name="media_id" value="{{$game->id}}">
                                        <button type="submit" class="btn btn-primary text-white" data-toggle="tooltip" data-placement="right" title="Add to watchlist"><i class="fas fa-plus"></i> Watchlist</a>
                                    </form>
                                @endif
                            </td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
                <div class="w-100 text-center bg-dark">
                    <a href="/media/create?type=3&genre={{$genre->id}}" class="btn btn-dark w-100 shadow-none">Add <i class="fas fa-plus"></i></a>
                </div>
            </section>
        </article>
        @endif
    @endforeach
@endsection