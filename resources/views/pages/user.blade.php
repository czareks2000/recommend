@extends('layouts.app')

@section('content')
<div class="container">
    @include('inc.messeges')
    <div class="row">
        <div class="col-12 mb-4 pt-5 pb-4 text-center bg-dark rounded shadow border">
            <img src="{{ asset('storage/avatars/'.$user->avatar_image)}}" alt="User avatar" width="150"><br>
            <h3 class="mt-3 text-white">{{ $user->name }}</h3>
        </div>
        <div class="col-9">
            <article>
                @foreach ($activity as $item)
                <section class="bg-white rounded shadow px-4 pt-2 pb-4 mb-4">
                    {{$item->media()->first()->title}} {{$item->type()->first()->name}}  {{$item->created_at}}
                </section>
                @endforeach
            </article>
        </div>
        <div class="col-3">
            <section class="bg-white rounded shadow px-3 pt-3 pb-4 mb-4">
                <h4 class="text-center">Friends</h4>
                <hr>
                <p><img src="{{ asset('storage/avatars/'.Auth::user()->avatar_image)}}" alt="User avatar" width="23"> Cezary Stachurski</p>
                <p><img src="{{ asset('storage/avatars/'.Auth::user()->avatar_image)}}" alt="User avatar" width="23"> Cezary Stachurski</p>
            </section>
            <section class="bg-white rounded shadow px-3 pt-3 pb-3 mb-4">
                <h4 class="text-center">Reviewed</h4>
                <hr>
                @foreach ($reviewed as $item)
                <p><a class="media-link" href="/media/{{$item->media()->first()->id}}"><b><i class="fas fa-caret-right"></i> {{$item->media()->first()->title}}</b> ({{$item->rating}} <i class="fas fa-star text-primary"></i>)</a></p>
                @endforeach
            </section>
        </div>
    </div>
</div>
@endsection