@extends('layouts.app')

@section('content')
<div class="container">
    @include('inc.messeges')
    <article class="my-5">
        <a href="@if (old('url') === null) {{url()->previous()}} @else {{ old('url') }} @endif" class="btn btn-primary text-white mb-4"><i class="fas fa-chevron-left"></i> Go Back</a>
        <h2>Add New Title</h2>
        <section class="bg-white p-4 rounded shadow">
            <form class="row" action="/media" method="POST" enctype="multipart/form-data" autocomplete="off">
                @csrf
                <input type="hidden" name="url" value="@if (old('url') === null) {{url()->previous()}} @else {{ old('url') }} @endif">
                <div class="form-group col-12">
                    <label for="mediaType">Type</label>
                    <select class="custom-select  @error('mediaType') is-invalid @enderror" id="mediaType" name="mediaType">
                        <option selected hidden>Choose...</option>
                        @if (count($mediaTypes) > 0)
                            @foreach ($mediaTypes as $mediaType)
                            <option value="{{$mediaType->id}}" @if(old('mediaType') == $mediaType->id || $_GET['type'] == $mediaType->id) selected @endif>{{$mediaType->name}}</option> 
                            @endforeach
                        @endif
                    </select>
                    @error('mediaType')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div id="form" class="col-12">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" placeholder="Enter title" name="title" value="{{ old('title') }}">
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="genre">Genre</label>
                        <select class="custom-select @error('genre') is-invalid @enderror" id="genre" name="genre">
                            <option selected hidden>Choose...</option>
                        </select>
                        @error('genre')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div id="credit" class="form-group">
         
                        @error('director')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="link">Where to watch</label>
                        <input type="text" class="form-control @error('link') is-invalid @enderror" id="link" placeholder="Paste a link" name="link" value="{{ old('link') }}">
                        @error('link')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="description">Description (max 360 characters)</label>
                        <textarea class="form-control @error('description') is-invalid @enderror" id="description" placeholder="Enter descrpition" rows="4" name="description">{{ old('description') }}</textarea>
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        Cover Image (Movie poster format)
                        <div class="custom-file mt-2">
                            <input type="file" class="custom-file-input @error('cover_image') is-invalid @enderror" id="file" name="cover_image"  value="{{ old('cover_image') }}">
                            <label class="custom-file-label" for="file">Choose file</label>
                            @error('cover_image')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary text-white px-5">Confirm</button>
                    </div>
                </div>
            </form>
            <script>
                //name of the file appear on select
                $(".custom-file-input").on("change", function() {
                  var fileName = $(this).val().split("\\").pop();
                  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                });
                $('#form').hide();
                $(document).ready(function(){
                    @if (old('mediaType') !== null || isset($_GET['type']))
                        $.ajax({
                            url: '/genres/'+$('#mediaType').val(),
                            type: 'GET',
                            dataType: 'json',
                            success: function(response){
                                
                                for (const [key, value] of Object.entries(response['genres'])) {
                                    if(key == "{{old('genre')}}" || key == "{{$_GET['genre']}}")
                                        $("#genre").append(new Option(value, key, false, true));
                                    else
                                        $("#genre").append(new Option(value, key));
                                }
                            }
                        });
                        showCreditInput();
                        $('#form').show();
                    @endif
                    $('#mediaType').change(function(){
                        $('#form').slideUp();
                        setTimeout(
                        function() 
                        {
                            showCreditInput();
                            $('#form').slideDown();
                        }, 300);
                    });
                });

                function showCreditInput() {
                        $('#genre').children('option:not(:first)').remove();
                            var mediaType_id = $('#mediaType').val();

                            switch (mediaType_id) {
                                case '1':
                                    $('#credit').prepend('<label for="director">Director</label><input type="text" class="form-control @error("director") is-invalid @enderror" id="director" placeholder="Enter director name" name="director" value="{{ old("director") }}">');
                                    break;

                                case '2':
                                    $('#credit').prepend('<label for="network">Network</label><input type="text" class="form-control @error("director") is-invalid @enderror" id="network" placeholder="Enter network name" name="director" value="{{ old("director") }}">');
                                    
                                    break;

                                case '3':
                                    $('#credit').prepend('<label for="developer">Developer</label><input type="text" class="form-control @error("director") is-invalid @enderror" id="developer" placeholder="Enter developer name" name="director" value="{{ old("director") }}">');
                                    
                                    break;

                                case '4':
                                    $('#credit').prepend('<label for="author">Author</label><input type="text" class="form-control @error("director") is-invalid @enderror" id="Author" placeholder="Enter author name" name="director" value="{{ old("director") }}">');
                                    
                                    break;
                            
                                default:
                                    break;
                            }

                            $.ajax({
                                url: '/genres/'+mediaType_id,
                                type: 'GET',
                                dataType: 'json',
                                success: function(response){
                                    
                                    for (const [key, value] of Object.entries(response['genres'])) {
                                        $("#genre").append(new Option(value, key));
                                    }
                                }
                            }); 
                    }
            </script>
        </section>
    </article>
</div>
@endsection