@extends('layouts.app')

@section('content')
<div class="container">
   
    <article class="my-4">
        <a href="@if (session('previous_url')) {{session('previous_url')}} @else {{url()->previous()}} @endif" class="btn btn-primary text-white mb-4"><i class="fas fa-chevron-left"></i> Go Back</a>
        @include('inc.messeges')
        <h2>Review of the {{$media->title}}</h2>
        <section class="bg-white p-4 rounded shadow">
            <form class="row" action="/review" method="POST" autocomplete="off">
                @csrf
                <input type="hidden" name="media_id" value="{{$media->id}}">
                <input type="hidden" name="url" value="@if (session('previous_url')) {{session('previous_url')}} @else {{url()->previous()}} @endif">
                <div id="form" class="col-12">
                    <h4>YOUR RATING</h4>
                    <div class="rating">
                        <label>
                            <input type="radio" name="rating" value="1" />
                            <span class="icon"><i class="fas fa-star"></i></span>
                        </label>
                        <label>
                            <input type="radio" name="rating" value="2" />
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                        </label>
                        <label>
                            <input type="radio" name="rating" value="3" />
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>   
                        </label>
                        <label>
                            <input type="radio" name="rating" value="4" />
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                        </label>
                        <label>
                            <input type="radio" name="rating" value="5" />
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                        </label>
                        <label>
                            <input type="radio" name="rating" value="6" />
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                        </label>
                        <label>
                            <input type="radio" name="rating" value="7" />
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                        </label>
                        <label>
                            <input type="radio" name="rating" value="8" />
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                        </label>
                        <label>
                            <input type="radio" name="rating" value="9" />
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                        </label>
                        <label>
                            <input type="radio" name="rating" value="10" />
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                        </label>
                    </div>
                    
                    <h4>YOUR REVIEW</h4>
                    <div class="form-group">
                        <textarea class="form-control @error('description') is-invalid @enderror" placeholder="Write your review here (optional)" rows="4" name="description">{{ old('description') }}</textarea>
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary text-white px-5">Submit</button>
                    </div>
                </div>
            </form>
        </section>
    </article>
</div>
@endsection