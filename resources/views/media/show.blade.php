@extends('layouts.app')

@section('content')
<div class="container">
    @include('inc.messeges')
    <article class="mb-5">
            <div class="table-responsive bg-white rounded shadow">
                <table>
                    <tbody>
                        <tr>
                            <td class="m-0 p-0 d-none d-md-block"><a class="media-link" href="/media/{{$media->id}}"><img class="img-main" width="300" src="/storage/cover_images/{{$media->image}}" alt="{{$media->title}}"></a></td>
                            <td class="p-4 align-top">
                                <h2><b>{{$media->title}}</b> <i class="fas fa-star text-primary"></i> {{$media->rating}}</h2>
                                <span>Recommended by 234 users</span>
                                <h5 class="mt-5"><b>Genre:</b> {{$media->genre()->first()->name}}</h5>
                                <h5><b>
                                @switch($media->mediaType_id)
                                    @case(1)
                                        Director:
                                        @break
                                    @case(2)
                                        Network:
                                        @break
                                    @case(3)
                                        Developer:
                                        @break
                                    @case(4)
                                        Author:
                                        @break
                                @endswitch
                                </b> {{$media->director}}</h5>
                                <h5><b>Where to watch:</b> <a href="{{$media->link}}" target="_blank" class="text-dark">link</a></h5>
                                <h5><b>Description:</b></h5>
                                <p>{{$media->description}}</p>
                                <div class="d-flex">
                                    <a class="btn btn-primary text-white mr-1" data-toggle="tooltip" data-placement="bottom" title="Recommend to friend" href="/media/{{$media->id}}/recommend">Recommend <i class="fas fa-share"></i></a>
                                    @if (count(App\Watchlist::where('user_id', Auth::user()->id)->where('media_id', $media->id)->get()) > 0)
                                        <form class="m-0 p-0 mr-1" action="/watchlist/{{App\Watchlist::where('user_id', Auth::user()->id)->where('media_id', $media->id)->first()->id}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <input type="hidden" name="url" value="/media/{{$media->id}}">
                                            <input type="hidden" name="media_id" value="{{$media->id}}">
                                            <button type="submit" class="btn btn-primary text-white" data-toggle="tooltip" data-placement="bottom" title="Remove from watchlist"><i class="fas fa-minus-square"></i> Watchlist</button>
                                        </form>
                                    @else
                                        <form class="m-0 p-0 mr-1" action="/watchlist" method="POST">
                                            @csrf
                                            <input type="hidden" name="url" value="/media/{{$media->id}}">
                                            <input type="hidden" name="media_id" value="{{$media->id}}">
                                            <button type="submit" class="btn btn-primary text-white" data-toggle="tooltip" data-placement="bottom" title="Add to watchlist"><i class="fas fa-plus"></i> Watchlist</a>
                                        </form>
                                    @endif
                                    @if (Auth::user()->admin == 1 ||  Auth::user()->id == $media->user_id)
                                        <a class="btn btn-primary text-white mr-1" data-toggle="tooltip" data-placement="bottom" title="Edit" href="/media/{{$media->id}}/edit">Edit <i class="fas fa-pen"></i></a>         
                                    @endif
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
    </article>

    <article>
        <section class="bg-white px-4 py-4 rounded shadow">
            <ul class="col-12 nav nav-tabs mb-3" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                  <a class="nav-link text-dark active" id="pills-reviews-tab" data-toggle="pill" href="#pills-reviews" role="tab" aria-controls="pills-reviews" aria-selected="true">Reviews</a>
                </li>
                <li class="nav-item" role="presentation">
                  <a class="nav-link text-dark" id="pills-more-tab" data-toggle="pill" href="#pills-more" role="tab" aria-controls="pills-more" aria-selected="false">More Like This</a>
                </li>
            </ul>
            <div class="col-12 tab-content pt-3" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-reviews" role="tabpanel" aria-labelledby="pills-reviews-tab">
                    @foreach ($reviews as $item)
                        <div class="media row">
                            <div class="col-12 col-md-2 text-center mr-3">
                                <a class="media-link" href="/user/{{$item->user()->first()->id}}">
                                <img src="{{ asset('storage/avatars/'.$item->user()->first()->avatar_image)}}" width="80" class="m-auto" alt="{{$item->user()->first()->name}}">
                                <p class="pt-2">{{$item->user()->first()->name}}</p>
                                </a>
                            </div>
                            <div class="col-12 col-md-10 media-body p-3 border rounded mb-5">
                                <h5 class="mt-0">
                                    @for ($i = 0; $i < $item->rating; $i++)
                                        <i class="fas fa-star text-primary"></i>
                                    @endfor
                                    @for ($i = $item->rating; $i < 10; $i++)
                                        <i class="fas fa-star text-dark"></i>
                                    @endfor
                                </h5>
                                @if ($item->description == NULL)
                                    <i>No description</i>
                                @else
                                    {{$item->description}}
                                @endif
                                <br>
                                <small>{{$item->updated_at}}</small>
                            </div>
                        </div>
                    @endforeach
                    <div class="text-center">
                        @if (($review = App\Review::where('media_id', $media->id)->where('user_id', Auth::user()->id)->first()) == NULL)
                            <a class="btn btn-primary text-white ml-1" data-toggle="tooltip" data-placement="bottom" title="Write a review" href="/media/{{$media->id}}/review">Write a review <i class="fas fa-plus"></i></a>
                        @else
                            <a class="btn btn-primary text-white ml-1" data-toggle="tooltip" data-placement="bottom" title="Edit your review" href="/review/{{$review->id}}/edit">Edit your review <i class="fas fa-pen"></i></a>
                        @endif
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-more" role="tabpanel" aria-labelledby="pills-more-tab">
                    @if (count($simillar) > 1)
                    <table class="table table-striped mt-0 pt-0">
                        <thead>
                            <tr class="m-0">
                                <th style="width: 1px; white-space: nowrap; border-top: none"></th>
                                <th style="border-top: none"></th>
                                <th style="border-top: none"></th>
                                <th style="border-top: none"></th>
                                <th style="width: 1px; white-space: nowrap; border-top: none"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($simillar as $item)
                                @if ($item->id != $media->id)
                                <tr>
                                    <td class="m-0 p-0"><a class="media-link" href="/media/{{$item->id}}"><img class="img-poster" width="60" src="/storage/cover_images/{{$item->image}}" alt="{{$item->title}}"></a></td>
                                    <td class="align-middle"><a class="media-link" href="/media/{{$item->id}}">{{$item->title}}</a></td>
                                    <td class="align-middle">{{$item->director}}</td>
                                    <td class="align-middle"><i class="fas fa-star text-primary"></i> {{$item->rating}}</td>
                                    <td class="align-middle">
                                        @if (count(App\Watchlist::where('user_id', Auth::user()->id)->where('media_id', $item->id)->get()) > 0)
                                            <form class="m-0 p-0" action="/watchlist/{{App\Watchlist::where('user_id', Auth::user()->id)->where('media_id', $item->id)->first()->id}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <input type="hidden" name="url" value="/media/{{$media->id}}">
                                                <input type="hidden" name="media_id" value="{{$item->id}}">
                                                <button type="submit" class="btn btn-primary text-white" data-toggle="tooltip" data-placement="right" title="Remove from watchlist"><i class="fas fa-minus-square"></i> Watchlist</button>
                                            </form>
                                        @else
                                            <form class="m-0 p-0" action="/watchlist" method="POST">
                                                @csrf
                                                <input type="hidden" name="url" value="/media/{{$media->id}}">
                                                <input type="hidden" name="media_id" value="{{$item->id}}">
                                                <button type="submit" class="btn btn-primary text-white" data-toggle="tooltip" data-placement="right" title="Add to watchlist"><i class="fas fa-plus"></i> Watchlist</a>
                                            </form>
                                        @endif
                                    </td>
                                </tr>                    
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        <p>There are no similar ones in the database</p>
                    @endif
                </div>
            </div>
        </section>
    </article>
           
</div>
@endsection