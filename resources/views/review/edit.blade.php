@extends('layouts.app')

@section('content')
@include('inc.messeges')
<div class="container">
    @include('inc.messeges')
    <article class="my-4">
        <a href="{{url()->previous()}}" class="btn btn-primary text-white mb-4"><i class="fas fa-chevron-left"></i> Go Back</a>
        <h2>Edit Review of the {{$media->title}}</h2>
        <section class="bg-white p-4 rounded shadow">
            <form class="row" action="review/{{$review->id}}/edit" method="POST" autocomplete="off">
                @csrf
                @method('PUT')
                <input type="hidden" name="media_id" value="{{$media->id}}">
                <input type="hidden" name="url" value="{{url()->previous()}}">
                <div id="form" class="col-12">
                    <h4>YOUR RATING</h4>
                    <div class="rating">
                        <label>
                            <input type="radio" name="rating" value="1" @if ($review->rating == 1.0) checked @endif/>
                            <span class="icon"><i class="fas fa-star"></i></span>
                        </label>
                        <label>
                            <input type="radio" name="rating" value="2" @if ($review->rating == 2.0) checked @endif/>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                        </label>
                        <label>
                            <input type="radio" name="rating" value="3" @if ($review->rating == 3.0) checked @endif/>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>   
                        </label>
                        <label>
                            <input type="radio" name="rating" value="4" @if ($review->rating == 4.0) checked @endif/>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                        </label>
                        <label>
                            <input type="radio" name="rating" value="5" @if ($review->rating == 5.0) checked @endif/>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                        </label>
                        <label>
                            <input type="radio" name="rating" value="6" @if ($review->rating == 6.0) checked @endif/>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                        </label>
                        <label>
                            <input type="radio" name="rating" value="7" @if ($review->rating == 7.0) checked @endif/>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                        </label>
                        <label>
                            <input type="radio" name="rating" value="8" @if ($review->rating == 8.0) checked @endif/>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                        </label>
                        <label>
                            <input type="radio" name="rating" value="9" @if ($review->rating == 9.0) checked @endif/>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                        </label>
                        <label>
                            <input type="radio" name="rating" value="10" @if ($review->rating == 10.0) checked @endif/>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                            <span class="icon"><i class="fas fa-star"></i></span>
                        </label>
                    </div>
                    
                    <h4>YOUR REVIEW</h4>
                    <div class="form-group">
                        <textarea class="form-control" placeholder="Write your review here (optional)" rows="4" name="description">{{$review->description}}</textarea>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary text-white px-5">Submit</button>
                    </div>
                </div>
            </form>
        </section>
    </article>
</div>
@endsection