@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-10">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-xl-4 col-form-label text-xl-right">{{ __('Avatar') }}</label>

                            <!-- Select option avatar images -->
                            <div id="avatars" class="col-xl-6">
                                <select class="image-picker show-html" name="avatar_image" hidden>
                                    <option data-img-src="storage/avatars/avatar1.png" data-img-class="first" data-img-alt="Avatar 1" value="avatar1.png" @if(old('avatar_image') == "avatar1.png") selected @endif>  Avatar 1  </option>
                                    <option data-img-src="storage/avatars/avatar2.png" data-img-alt="Avatar 2" value="avatar2.png" @if(old('avatar_image') == "avatar2.png") selected @endif>  Avatar 2  </option>
                                    <option data-img-src="storage/avatars/avatar3.png" data-img-alt="Avatar 3" value="avatar3.png" @if(old('avatar_image') == "avatar3.png") selected @endif>  Avatar 3  </option>
                                    <option data-img-src="storage/avatars/avatar4.png" data-img-alt="Avatar 4" value="avatar4.png" @if(old('avatar_image') == "avatar4.png") selected @endif>  Avatar 4  </option>
                                    <option data-img-src="storage/avatars/avatar5.png" data-img-alt="Avatar 5" value="avatar5.png" @if(old('avatar_image') == "avatar5.png") selected @endif>  Avatar 5  </option>
                                    <option data-img-src="storage/avatars/avatar6.png" data-img-alt="Avatar 6" value="avatar6.png" @if(old('avatar_image') == "avatar6.png") selected @endif>  Avatar 6  </option>
                                    <option data-img-src="storage/avatars/avatar7.png" data-img-alt="Avatar 7" value="avatar7.png" @if(old('avatar_image') == "avatar7.png") selected @endif>  Avatar 7  </option>
                                    <option data-img-src="storage/avatars/avatar8.png" data-img-alt="Avatar 8" value="avatar8.png" @if(old('avatar_image') == "avatar8.png") selected @endif>  Avatar 8  </option>
                                    <option data-img-src="storage/avatars/avatar9.png" data-img-alt="Avatar 9" value="avatar9.png" @if(old('avatar_image') == "avatar9.png") selected @endif>  Avatar 9  </option>
                                    <option data-img-src="storage/avatars/avatar10.png" data-img-alt="Avatar 10" value="avatar10.png" @if(old('avatar_image') == "avatar10.png") selected @endif>  Avatar 10  </option>
                                    <option data-img-src="storage/avatars/avatar11.png" data-img-alt="Avatar 11" value="avatar11.png" @if(old('avatar_image') == "avatar11.png") selected @endif>  Avatar 11  </option>
                                    <option data-img-src="storage/avatars/avatar12.png" data-img-alt="Avatar 12" value="avatar12.png" @if(old('avatar_image') == "avatar12.png") selected @endif>  Avatar 12  </option>
                                    <option data-img-src="storage/avatars/avatar13.png" data-img-alt="Avatar 13" value="avatar13.png" @if(old('avatar_image') == "avatar13.png") selected @endif>  Avatar 13  </option>
                                    <option data-img-src="storage/avatars/avatar14.png" data-img-alt="Avatar 14" value="avatar14.png" @if(old('avatar_image') == "avatar14.png") selected @endif>  Avatar 14  </option>
                                    <option data-img-src="storage/avatars/avatar15.png" data-img-alt="Avatar 15" value="avatar15.png" @if(old('avatar_image') == "avatar15.png") selected @endif>  Avatar 15  </option>
                                    <option data-img-src="storage/avatars/avatar16.png" data-img-class="last" data-img-alt="Avatar 16" value="avatar16.png" @if(old('avatar_image') == "avatar16.png") selected @endif>  Avatar 16  </option>
                                </select>
                                <script>
                                    $(document).ready(function () {
                                        $("select").imagepicker();
                                        $("#avatars img").css('width', '50px');
                                    });
                                </script>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-xl-4 col-form-label text-xl-right">{{ __('Name') }}</label>

                            <div class="col-xl-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-xl-4 col-form-label text-xl-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-xl-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-xl-4 col-form-label text-xl-right">{{ __('Password') }}</label>

                            <div class="col-xl-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-xl-4 col-form-label text-xl-right">{{ __('Confirm Password') }}</label>

                            <div class="col-xl-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-xl-6 offset-xl-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
