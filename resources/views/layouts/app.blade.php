<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @isset($title)
    <title>{{ $title }}</title>
    @else
    <title>{{ config('app.name', 'Laravel') }}</title>
    @endisset

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js" integrity="sha512-WNLxfP/8cVYL9sj8Jnp6et0BkubLP31jhTG9vhL/F5uEZmg5wEzKoXp1kJslzPQWwPT1eyMiSxlKCgzHLOTOTQ==" crossorigin="anonymous"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg bg-dark shadow-sm">
            <div class="container">
                <a class="navbar-brand text-white" href="{{ url('/') }}">
                    <i class="fas fa-star text-primary"></i> {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-dark navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon text-secondary"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    @auth
                    <ul class="navbar-nav mr-auto ml-lg-5">
                        <form class="form-inline my-2 my-lg-0">
                            <div class="input-group">
                                <div class="input-group-prepend text-center"> 
                                    <select class="btn btn-light shadow-none" id="inputGroupSelect01">
                                        <option value="1">All</option>
                                        <option value="2">Films</option>
                                        <option value="3">Users</option>
                                      </select>
                                </div>
                                <input class="form-control shadow-none" type="search" placeholder="Search" aria-label="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-primary shadow-none" type="button"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </ul>                
                    @endauth

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i> {{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}"><i class="fas fa-user-plus"></i> {{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('home') ? 'text-primary' : '' }}" href="/home"><i class="fas fa-home"></i> Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('/') ? 'text-primary' : '' }}" href="/"><i class="fas fa-compass"></i> Browse</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('watchlist') ? 'text-primary' : '' }}" href="/watchlist"><i class="fas fa-bookmark"></i> Watchlist</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="fas"><img src="{{ asset('storage/avatars/'.Auth::user()->avatar_image)}}" alt="User avatar" width="20"></i> {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="/user/{{Auth::user()->id}}">
                                        <i class="fas fa-user"></i> Profile
                                    </a>
                                    <a class="dropdown-item" href="/settings">
                                        <i class="fas fa-cog"></i> Settings
                                     </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <footer class="text-center p-3 bg-white">
        All rights reserved &copy; 2020 Cezary Stachurski
    </footer>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
        var w0 = $('.img-main').width() * 1.48;
        $('.img-main').css('object-fit', 'cover');
        $('.img-main').css({'height':w0+'px'});

        var w1 = $('.img-poster').width() * 1.48;
        $('.img-poster').css('object-fit', 'cover');
        $('.img-poster').css({'height':w1+'px'});
        
        var w2 = $('.img-2').width() / 2;
        $('.img-2').css('object-fit', 'cover');
        $('.img-2').css({'height':w2+'px'});

        var w3 = $('.img-4').width() / 4;
        $('.img-4').css('object-fit', 'cover');
        $('.img-4').css({'height':w3+'px'});

        var w4 = $('.img-square').width();
        $('.img-square').css('object-fit', 'cover');
        $('.img-square').css({'height':w4+'px'});

        $(document).ready(function () 
        {   
            $( window ).resize(function() {
                w0 = $('.img-main').width()  * 1.48;
                $('.img-main').css({'height':w0+'px'});
            });

            $( window ).resize(function() {
                w1 = $('.img-poster').width()  * 1.48;
                $('.img-poster').css({'height':w1+'px'});
            });

            
            $( window ).resize(function() {
                w2 = $('.img-2').width() / 2;
                $('.img-2').css({'height':w2+'px'});
            });      

            
            $( window ).resize(function() {
                w3 = $('.img-4').width() / 4;
                $('.img-4').css({'height':w3+'px'});
            });  

            
            $( window ).resize(function() {
                w4 = $('.img-square').width();
                $('.img-square').css({'height':w4+'px'});
            });               
        });
    </script>
</body>
</html>
