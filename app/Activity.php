<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    public function type()
    {
        return $this->belongsTo('App\ActivityType', 'activity_id');
    }

    public function media()
    {
        return $this->belongsTo('App\Media');
    }
}
