<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;

class AjaxController extends Controller
{
    public function getGenres($mediaType_id)
    {
        $genres = Genre::where('mediaType_id', $mediaType_id)->get();

        $result = array();

        foreach ($genres as $genre ) {
            $result[$genre->id] = $genre->name;
        }

        return response()->json(array('success' => true, 'genres' => $result));
    }
}
