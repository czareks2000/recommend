<?php

namespace App\Http\Controllers;

use App\Media;
use App\User;
use App\Review;
use App\Genre;
use App\Activity;
use Illuminate\Http\Request;

class PageController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function browse()
    {   
        $title = "Browse";
        $movies = Media::where('mediaType_id', 1)->orderBy('rating', 'desc')->limit(4)->get();
        $series = Media::where('mediaType_id', 2)->orderBy('rating', 'desc')->limit(4)->get();
        $games = Media::where('mediaType_id', 3)->orderBy('rating', 'desc')->limit(4)->get();
        $books = Media::where('mediaType_id', 4)->orderBy('rating', 'desc')->limit(4)->get();

        return view('pages.browse')->with('title', $title)
                        ->with('movies', $movies)
                        ->with('series', $series)
                        ->with('games', $games)
                        ->with('books', $books);
    }

    public function movies()
    {   
        $title = "Movies";
        $movies = Media::where('mediaType_id', 1)->orderBy('title', 'asc')->get();
        $genres = Genre::where('mediaType_id', 1)->orderBy('name', 'asc')->get();
        return view('pages.movies')->with('title', $title)
                                ->with('genres', $genres)
                                ->with('movies', $movies);
    }

    public function series()
    {   
        $title = "Series";
        $series = Media::where('mediaType_id', 2)->orderBy('title', 'asc')->get();
        $genres = Genre::where('mediaType_id', 2)->orderBy('name', 'asc')->get();
        return view('pages.series')->with('title', $title)
                                ->with('genres', $genres)
                                ->with('series', $series);
    }

    public function games()
    {   
        $title = "Games";
        $games = Media::where('mediaType_id', 3)->orderBy('title', 'asc')->get();
        $genres = Genre::where('mediaType_id', 3)->orderBy('name', 'asc')->get();
        return view('pages.games')->with('title', $title)
                                ->with('genres', $genres)
                                ->with('games', $games);
    }

    public function books()
    {   
        $title = "Books";
        $books = Media::where('mediaType_id', 4)->orderBy('title', 'asc')->get();
        $genres = Genre::where('mediaType_id', 4)->orderBy('name', 'asc')->get();
        return view('pages.books')->with('title', $title)
                                ->with('genres', $genres)
                                ->with('books', $books);
    }

    public function home()
    {   
        $title = "Home";
        return view('pages.home')->with('title', $title);
    }

    public function user(User $user)
    {
        $title = $user->name;
        $activity = Activity::where('user_id', $user->id)->get();
        $reviewed = Review::where('user_id', $user->id)->orderBy('updated_at', 'desc')->get();
        return view('pages.user')->with('title', $title)
                                    ->with('user', $user)
                                    ->with('activity', $activity)
                                    ->with('reviewed', $reviewed);
    }

    public function settings()
    {   
        $title = "Settings";
        return view('pages.settings')->with('title', $title);
    }
    
}
