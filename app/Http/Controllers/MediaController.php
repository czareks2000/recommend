<?php

namespace App\Http\Controllers;

use App\Media;
use App\Review;
use App\MediaType;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class MediaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $mediaTypes = MediaType::all();
        return view('media.create')->with('mediaTypes', $mediaTypes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //validation
        $this->validate($request, [
            'mediaType' => 'integer|required',
            'title' => ['required', Rule::unique('media')->where(function ($query) use($request) {
                return $query->where('mediaType_id', $request->input('mediaType'));
            })],
            'genre' => 'integer|required',
            'director'=> 'required',
            'link' => 'required',
            'description' => 'required|max:360',
            'cover_image' => 'image|required|max:1999'
        ]);

        //image handle
        if($request->hasFile('cover_image')){
            $fileNameWithExt = $request->file('cover_image')->getClientOriginalName();
            
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            $extension = $request->file('cover_image')->getClientOriginalExtension();

            $fileNameToStore = $fileName.'_'.time().'.'.$extension;

            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);
        }
        else{
            $fileNameToStore = 'noimage.jpg';
        }

        //create media
        $media = new Media;
        $media->user_id = auth()->user()->id;
        $media->mediaType_id = $request->input('mediaType');
        $media->title = $request->input('title');
        $media->director = $request->input('director');
        $media->description = $request->input('description');
        $media->genre_id = $request->input('genre');
        $media->link = $request->input('link');
        $media->image = $fileNameToStore;
        $media->rating = 0.0;
        $media->save();

        switch ($media->mediaType_id) {
            case 1:
                return redirect('/movies')->with('success', 'The movie <b>'.$request->input('title').'</b> was successfully added to the database.');
                break;
            case 2:
                return redirect('/series')->with('success', 'The series <b>'.$request->input('title').'</b> was successfully added to the database.');
                break;
            case 3:
                return redirect('/games')->with('success', 'The game <b>'.$request->input('title').'</b> was successfully added to the database.');
                break;
            case 4:
                return redirect('/books')->with('success', 'The book <b>'.$request->input('title').'</b> was successfully added to the database.');
                break;
            default:
                return redirect('/')->with('success', 'The <b>'.$request->input('title').'</b> was successfully added to the database.');
                break;
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function show(Media $media)
    {   
        $title = $media->title;
        $simillar = Media::where('genre_id', $media->genre_id)->where('mediaType_id', $media->mediaType_id)->get();
        $reviews = Review::where('media_id', $media->id)->get();

        return view('media.show')->with('title', $title)
                            ->with('simillar', $simillar)
                            ->with('reviews', $reviews)
                            ->with('media', $media);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function edit(Media $media)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Media $media)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function destroy(Media $media)
    {
        //
    }
}
