<?php

namespace App\Http\Controllers;

use App\Review;
use App\Media;
use App\Activity;
use Illuminate\Http\Request;

class ReviewController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function create(Media $media)
    {
        $title = $media->title;

        return view('media.review')->with('title', $title)
                                    ->with('media', $media);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $previous_url = $request['url'];

        if($request['rating'] == NULL)
        {   
            return redirect()->route('review.create', $request['media_id'])->with('previous_url', $previous_url)->with('error', 'Rating is required');
        }

        $media = Media::find($request['media_id']);

        $reviews = Review::where('media_id', $request['media_id'])->get();

        if(count($reviews) == 0)
        {
            $media->rating = $request['rating'];
        }
        else
        {   
            $sum = $request['rating'];
            $number = 1;

            foreach ($reviews as $item) {
                $sum += $item->rating;
                $number++;
            }

            $media->rating = $sum / $number;
        }
        
        $media->save();

        $review = new Review;
        $review->user_id = auth()->user()->id;
        $review->rating = $request['rating'];
        $review->description = $request['description'];
        $review->media_id = $request['media_id'];
        $review->save();

        $activity = new Activity;
        $activity->user_id = auth()->user()->id;
        $activity->activity_id = 3;
        $activity->media_id = $request['media_id'];
        $activity->save();

        return redirect($previous_url)->with('success', 'Review of the <b>'.$media->title.'</b> with rating: <b>'.$request['rating'].'/10</b> was succesfuly submited');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function show(Review $review)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        $title = "Edit review";
        $media = Media::find($review->media_id);
        return view('review.edit')->with('title', $title)
                                    ->with('review', $review)
                                    ->with('media', $media);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Review $review)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        //
    }
}
