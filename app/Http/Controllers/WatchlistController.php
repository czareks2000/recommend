<?php

namespace App\Http\Controllers;

use App\Watchlist;
use App\Media;
use App\Activity;
use DB;
use Illuminate\Http\Request;

class WatchlistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Watchlist";

        $watchlist = DB::table('watchlist')
            ->join('media', 'watchlist.media_id', '=', 'media.id')
            ->join('genres', 'media.genre_id', '=', 'genres.id')
            ->select('media.id', 'media.title', 'media.director', 'media.description', 'genres.name', 'media.link', 'media.image', 'media.rating')
            ->where('watchlist.user_id', '=', auth()->user()->id)
            ->orderBy('media.title')
            ->get();

        return view('pages.watchlist')->with('title', $title)
                                    ->with('watchlist', $watchlist);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $watchlist = new Watchlist;
        $watchlist->user_id = auth()->user()->id;
        $watchlist->media_id = $request['media_id'];
        $watchlist->save();
        
        $activity = new Activity;
        $activity->user_id = auth()->user()->id;
        $activity->activity_id = 1;
        $activity->media_id = $request['media_id'];
        $activity->save();


        return redirect($request['url'])->with('success', '<b>'.Media::find($request['media_id'])->title.'</b> was added to watchlist');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Watchlist  $watchlist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Watchlist $watchlist)
    {
        $watchlist->delete();

        return redirect($request['url'])->with('success', '<b>'.Media::find($request['media_id'])->title.'</b> was removed from watchlist');
    }
}
