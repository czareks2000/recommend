<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';
    public $timestamps = false;

    public function genre()
    {
        return $this->belongsTo('App\Genre');
    }

}
