<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'PageController@browse');
Route::get('/movies', 'PageController@movies');
Route::get('/series', 'PageController@series');
Route::get('/games', 'PageController@games');
Route::get('/books', 'PageController@books');
Route::get('/home', 'PageController@home');
Route::get('/profile', 'PageController@profile');
Route::get('/settings', 'PageController@settings');

Route::get('/user/{user}', 'PageController@user');

Route::resource('media', 'MediaController')->only(['store','create']);
Route::put('media/{media}','MediaController@update');
Route::get('media/{media}','MediaController@show');
Route::delete('media/{media}','MediaController@destroy');
Route::get('media/{media}/edit','MediaController@edit');


Route::get('/genres/{id}', 'AjaxController@getGenres');

Route::resource('watchlist', 'WatchlistController')->only(['index','store','destroy']);

Route::resource('review', 'ReviewController')->except([
    'create'
]);

Route::get('/media/{media}/review', 'ReviewController@create')->name('review.create');