<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGenresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('genres', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->bigInteger('mediaType_id')->unsigned();
            $table->foreign('mediaType_id')->references('id')->on('media_types');
        });

        DB::table('genres')->insert([
            ['name' => 'Action', 'mediaType_id' => 1],
            ['name' => 'Adventure', 'mediaType_id' => 1],
            ['name' => 'Animation', 'mediaType_id' => 1],
            ['name' => 'Biography', 'mediaType_id' => 1],
            ['name' => 'Comedy', 'mediaType_id' => 1],
            ['name' => 'Crime', 'mediaType_id' => 1],
            ['name' => 'Documentary', 'mediaType_id' => 1],
            ['name' => 'Drama', 'mediaType_id' => 1],
            ['name' => 'Family', 'mediaType_id' => 1],
            ['name' => 'Fantasy', 'mediaType_id' => 1],
            ['name' => 'Film Noir', 'mediaType_id' => 1],
            ['name' => 'History', 'mediaType_id' => 1],
            ['name' => 'Horror', 'mediaType_id' => 1],
            ['name' => 'Music', 'mediaType_id' => 1],
            ['name' => 'Musical', 'mediaType_id' => 1],
            ['name' => 'Mystery', 'mediaType_id' => 1],
            ['name' => 'Romance', 'mediaType_id' => 1],
            ['name' => 'Sci-Fi', 'mediaType_id' => 1],
            ['name' => 'Sport', 'mediaType_id' => 1],
            ['name' => 'Superhero', 'mediaType_id' => 1],
            ['name' => 'Thriller', 'mediaType_id' => 1],
            ['name' => 'War', 'mediaType_id' => 1],
            ['name' => 'Western', 'mediaType_id' => 1],

            ['name' => 'Action', 'mediaType_id' => 2],
            ['name' => 'Adventure', 'mediaType_id' => 2],
            ['name' => 'Animation', 'mediaType_id' => 2],
            ['name' => 'Biography', 'mediaType_id' => 2],
            ['name' => 'Comedy', 'mediaType_id' => 2],
            ['name' => 'Crime', 'mediaType_id' => 2],
            ['name' => 'Documentary', 'mediaType_id' => 2],
            ['name' => 'Drama', 'mediaType_id' => 2],
            ['name' => 'Family', 'mediaType_id' => 2],
            ['name' => 'Fantasy', 'mediaType_id' => 2],
            ['name' => 'Game Show', 'mediaType_id' => 2],
            ['name' => 'History', 'mediaType_id' => 2],
            ['name' => 'Horror', 'mediaType_id' => 2],
            ['name' => 'Music', 'mediaType_id' => 2],
            ['name' => 'Musical', 'mediaType_id' => 2],
            ['name' => 'Mystery', 'mediaType_id' => 2],
            ['name' => 'News', 'mediaType_id' => 2],
            ['name' => 'Reality-TV', 'mediaType_id' => 2],
            ['name' => 'Romance', 'mediaType_id' => 2],
            ['name' => 'Sci-Fi', 'mediaType_id' => 2],
            ['name' => 'Sport', 'mediaType_id' => 2],
            ['name' => 'Superhero', 'mediaType_id' => 2],
            ['name' => 'Talk Show', 'mediaType_id' => 2],
            ['name' => 'Thriller', 'mediaType_id' => 2],
            ['name' => 'War', 'mediaType_id' => 2],
            ['name' => 'Western', 'mediaType_id' => 2],

            ['name' => 'Action', 'mediaType_id' => 3],
            ['name' => 'Adventure', 'mediaType_id' => 3],
            ['name' => 'Arcade', 'mediaType_id' => 3],
            ['name' => 'Casual', 'mediaType_id' => 3],
            ['name' => 'Indie', 'mediaType_id' => 3],
            ['name' => 'Racing', 'mediaType_id' => 3],
            ['name' => 'RPG', 'mediaType_id' => 3],
            ['name' => 'Shooter', 'mediaType_id' => 3],
            ['name' => 'Simulation', 'mediaType_id' => 3],
            ['name' => 'Sport', 'mediaType_id' => 3],
            ['name' => 'Strategy', 'mediaType_id' => 3],
            ['name' => 'Puzzle', 'mediaType_id' => 3],
            ['name' => 'Party', 'mediaType_id' => 3],
            ['name' => 'Platformer', 'mediaType_id' => 3],
            ['name' => 'Fighting', 'mediaType_id' => 3],
            ['name' => 'Survival', 'mediaType_id' => 3],
            ['name' => 'Horror', 'mediaType_id' => 3],
            ['name' => 'Stealth', 'mediaType_id' => 3],
            
            ['name' => 'Action', 'mediaType_id' => 4],
            ['name' => 'Adventure', 'mediaType_id' => 4],
            ['name' => 'Comedy', 'mediaType_id' => 4],
            ['name' => 'Crime', 'mediaType_id' => 4],
            ['name' => 'Drama', 'mediaType_id' => 4],
            ['name' => 'Famtasy', 'mediaType_id' => 4],
            ['name' => 'Historical', 'mediaType_id' => 4],
            ['name' => 'Historical fiction', 'mediaType_id' => 4],
            ['name' => 'Horror', 'mediaType_id' => 4],
            ['name' => 'Magical realism', 'mediaType_id' => 4],
            ['name' => 'Mystery', 'mediaType_id' => 4],
            ['name' => 'Paranoid fiction', 'mediaType_id' => 4],
            ['name' => 'Philosphical', 'mediaType_id' => 4],
            ['name' => 'Political', 'mediaType_id' => 4],
            ['name' => 'Romance', 'mediaType_id' => 4],
            ['name' => 'Saga', 'mediaType_id' => 4],
            ['name' => 'Satire', 'mediaType_id' => 4],
            ['name' => 'Science fiction', 'mediaType_id' => 4],
            ['name' => 'Social', 'mediaType_id' => 4],
            ['name' => 'Speculative', 'mediaType_id' => 4],
            ['name' => 'Thriller', 'mediaType_id' => 4],
            ['name' => 'Urban', 'mediaType_id' => 4],
            ['name' => 'Western', 'mediaType_id' => 4]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('genres');
    }
}
